#!/usr/bin/env python

from button import *
from subprocess import call
import time
import os

def disp(num):
    call(["curl", "-sSL", "http://10.0.0.101/" + str(num)])

def countdown():
    for i in range(5,0,-1):
        disp(i)
        time.sleep(1)
    disp("")


call(["gphoto2", "--set-config", "capturetarget=0"])
os.chdir('/home/kyle/photobooth')

b = Button(25)
while not b.is_pressed():
    pass
print("button pressed!")
countdown()
call(["gphoto2", "--capture-image-and-download"])
call(["mv", "capt0000.jpg", "capt1.jpg"])
countdown()
call(["gphoto2", "--capture-image-and-download"])
call(["mv", "capt0000.jpg", "capt2.jpg"])
countdown()
call(["gphoto2", "--capture-image-and-download"])
call(["mv", "capt0000.jpg", "capt3.jpg"])
print("done taking photos")

#!/bin/bash
cd /home/kyle/photobooth
mkdir -p archive/{photos,strips}
while [ true ]; do
  echo "waiting for button press"
  python buttonlistener.py
  echo "assembling strip"
  ./assemble_and_print
  echo "currently printing.."
done

/*
 * Start a webserver, count the number of visits.
 * Every visit, show the updated number on the display,
 * and in the resulting HTML page.
 */

#include <ESP8266WebServer.h>
#include <ESP8266NetBIOS.h>

#include <matrix.h>
#include <wifi.h>

ESP8266WebServer wwwserver(80);

static void five() {
  displayNum(5);
  wwwserver.send(200, "text/plain", "5");
}

static void four() {
  displayNum(4);
  wwwserver.send(200, "text/plain", "4");
}

static void three() {
  displayNum(3);
  wwwserver.send(200, "text/plain", "3");
}

static void two() {
  displayNum(2);
  wwwserver.send(200, "text/plain", "2");
}

static void one() {
  displayNum(1);
  wwwserver.send(200, "text/plain", "1");
}

static void zero() {
  displayNum(0);
  wwwserver.send(200, "text/plain", "0");
}

static void clear() {
  clearMatrix();
  wwwserver.send(200, "text/plain", "done");
}

void setup() {
  setupMatrix();
  connectWifi();

  wwwserver.on("/5", five);
  wwwserver.on("/4", four);
  wwwserver.on("/3", three);
  wwwserver.on("/2", two);
  wwwserver.on("/1", one);
  wwwserver.on("/0", zero);
  wwwserver.on("/", clear);
  wwwserver.begin();

  NBNS.begin("ESP");
}

void loop() {
  wwwserver.handleClient();
}
